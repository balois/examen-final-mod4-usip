import { Categoria } from '../models/Categoria.js';
import { Producto } from '../models/Producto.js';

export async function getCategorias(req, res) {
  try {
    const categorias = await Categoria.findAll({
      attributes: ['id', 'nombre'],
    });

    res.json(categorias);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function createCategoria(req, res) {
  console.log('Creando categoria', req.body);
  const { nombre} = req.body;
  try {
    const newCategoria = await Categoria.create(
      {
        nombre,
        
      },
      {
        fields: ['nombre'],
      }
    );
    return res.json(newCategoria);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getCategoria(req, res) {
  const { id } = req.params;
  try {
    const categoria = await Categoria.findOne({
      where: { id },
    });
    return res.json(categoria);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function updateCategoria(req, res) {
  const { id } = req.params;
  const { nombre } = req.body;

  try {
    const categoria = await Categoria.findByPk(id);
    categoria.nombre = nombre;

    await categoria.save();

    return res.json(categoria);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function deleteCategoria(req, res) {
  const { id } = req.params;
  try {
    await Producto.destroy({
      where: { categoria_id: id },
    });
    await Categoria.destroy({
      where: { id },
    });
    return res.sendStatus(204);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getCategoriaProductos(req, res) {
  const { id } = req.params;
  try {
    const productos = await Productos.findAll({
      attributes: ['id', 'categoria_id', 'nombre', 'precio_unitario'],
      where: { categoria_id: id },
    });
    return res.json(productos);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

export async function getCategoriasProductos(req, res) {
  try {
    const categorias = await Categoria.findAll({
      attributes: ['id', 'nombre'],
      include: [
        {
          model: Producto,
          attributes: ['id', 'nombre'],
          required: true,
        },
      ],
    });
    res.json(categorias);
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}
