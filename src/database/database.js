import { Sequelize } from 'sequelize';
export const sequelize = new Sequelize(
    'project_db', // DB NAME
    'postgres', //username
    'postgres', //pasword
    {
        host: 'localhost',
        dialect: 'postgres',
        port: "5433"
    }
);