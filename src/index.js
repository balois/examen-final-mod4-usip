import app from './app.js';
import { sequelize } from './database/database.js';
import 'dotenv/config';
import logger from './logs/logger.js';

async function main() {
  console.clear();
  await sequelize.sync({ force: false });
  //const PORT = process.env.PORT;
  const PORT = 3000;
  app.listen(3000);
  console.log('Server listening on ',3000);
}

main();

