import { DataTypes } from 'sequelize';
import { sequelize } from '../database/database.js';

export const Producto = sequelize.define('producto', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  nombre: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  precio_unitario: {
    type: DataTypes.INTEGER,
    defaultValue: 0,
  },
  estado: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
});
