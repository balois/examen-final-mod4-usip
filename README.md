# EXAMEN FINAL MODULO 4

1.- BASE DE DATOS

Para hacer correr el sistema: se debera crear la la base de datos denominada: project_db     en el Motor
de base de datos POSTGRES

## PREVIAMENTE SE DEBERA INSTALAR LAS DEPENDENCIAS QUE SE ENCUENTRAN EN EL ARCHIVO 

  package.json
  
  hacer correr con el siguiente comando
  npm run dev

## Propiedades de Conexion a la Base de datos

   user: "postgres",
  host: "localhost",
  database: "project_db",
  password: "postgres",
  port: "5433",


## El Sistema esta desarrollado en JavaScript con NODE.js

para lo cual es necesario instalar NODE.js y asi tambien el EXPRESS de Node como tambien las dependencias 

npm install express
npm install pg


## URL's para realizar la comprobacion del sistema por lo que se recomienda USAR POSTMAN

http://localhost:3000/api/productos
http://localhost:3000/api/categorias
http://localhost:3000/api/categorias

NOTA: no se pudo realizar el JWT ya que este tipo de programacion es totalmente nuevo para mi persona, pero de todas formas se hizo el CRUD, tambien se tuvo inconveniente en la lectura del puerto, por lo cual se hizo uso del puerto :3000

Muchas gracias por los conocimientos impartidos por su persona. de verdad que me abre las puertas a nuevos desafios